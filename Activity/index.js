// console.log("Hello World!");

let username;
let password;
let role;

function login () {
	let username = prompt("Enter your username: ");
	let password = prompt("Enter your password: ");
	let role = prompt("Enter your role: ").toLowerCase();

	if (username == " " || username == null || password == " " || password == null || role == " " || role == null)
	{
		alert("Input should not be empty!");
	}
	else 
	{
		switch (role) 
		{
			case 'admin':
			alert("Welcome back to the class portal, admin!");
			break;
			case 'teacher':
			alert("Thank you for loggin in, teacher!");
			break;
			case 'student':
			alert("Welcome to the class portal, student!");
			break;
			default:
			alert("Role out of range.");
			break;
		}
	}
}

login();

function averageOfNum (num1, num2, num3, num4) {
	let average = Math.round((num1 + num2 + num3 + num4) / 4);
	if (average <= 74) 
	{
		console.log("Hello, student, your average is: " + average + "." + " The letter equivalent is F.");
	}
	else if (average >= 75 && average <= 79) 
	{
		console.log("Hello, student, your average is: " + average + "." + " The letter equivalent is D.");
	}
	else if (average >= 80 && average <= 84) 
	{
		console.log("Hello, student, your average is: " + average + "." + " The letter equivalent is C.");
	}
	else if (average >= 85 && average <= 89)
	{
		console.log("Hello, student, your average is: " + average + "." + " The letter equivalent is B.");
	}
	else if (average >= 90 && average <=95)
	{
		console.log("Hello, student, your average is: " + average + "." + " The letter equivalent is A.");
	}
	else if (average > 96)
	{
		console.log("Hello, student, your average is: " + average + "." + " The letter equivalent is A+.");
	}
}
